package com.example.todo.ui

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.activityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.example.todo.R
import com.levibostian.recyclerviewmatcher.RecyclerViewMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class MainActivityTest {

    @get:Rule
    var activityScenarioRule = activityScenarioRule<MainActivity>()

    @Test
    fun changeText_sameActivity() {

        // Type text and then press the add button.
        onView(withId(R.id.inputTaskName))
            .perform(ViewActions.typeText(STRING_TO_BE_TYPED), ViewActions.closeSoftKeyboard())
        onView(withId(R.id.btnAdd)).perform(ViewActions.click())

        Thread.sleep(2000)

        // Check that the task was added to the list.
        onView(
            RecyclerViewMatcher.recyclerViewWithId(R.id.recyclerViewTasks)
                .viewHolderViewAtPosition(0, R.id.textViewTaskName)
        ).check(matches(withText(STRING_TO_BE_TYPED)))
    }

    companion object {
        const val STRING_TO_BE_TYPED = "Task 1"
    }

}