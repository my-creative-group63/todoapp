package com.example.todo.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.todo.databinding.ActivityMainTaskItemBinding
import com.example.todo.models.Task

class TaskAdapter internal constructor(private val tasks: MutableList<Task>) :
    RecyclerView.Adapter<TaskAdapter.TaskViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ActivityMainTaskItemBinding.inflate(layoutInflater, parent, false)
        return TaskViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        holder.binding.task = tasks[position]
    }

    override fun getItemCount(): Int {
        return tasks.size
    }

    inner class TaskViewHolder internal constructor(val binding: ActivityMainTaskItemBinding) :
        RecyclerView.ViewHolder(binding.root)

}