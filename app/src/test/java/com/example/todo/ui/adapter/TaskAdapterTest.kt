package com.example.todo.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.todo.databinding.ActivityMainTaskItemBinding
import com.example.todo.models.Task
import io.mockk.*
import io.mockk.impl.annotations.RelaxedMockK
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test

class TaskAdapterTest {

    @RelaxedMockK
    private lateinit var tasks: MutableList<Task>

    private lateinit var taskAdapter: TaskAdapter

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)

        taskAdapter = TaskAdapter(tasks)
    }

    @Test
    fun onCreateViewHolder() {
        val viewType = 1
        val parent = mockk<ViewGroup>()
        val context = mockk<Context>()
        val layoutInflater = mockk<LayoutInflater>()
        val binding = mockk<ActivityMainTaskItemBinding>()
        val root = mockk<View>()

        every { parent.context } returns context
        every { binding.root } returns root

        mockkStatic(LayoutInflater::class)
        every { LayoutInflater.from(context) } returns layoutInflater

        mockkStatic(ActivityMainTaskItemBinding::class)
        every { ActivityMainTaskItemBinding.inflate(layoutInflater, parent, false) } returns binding

        // When
        val taskViewHolder = taskAdapter.onCreateViewHolder(parent, viewType)

        // Then
        assertNotNull(taskViewHolder)
    }

    @Test
    fun onBindViewHolder() {
        // Given
        val position = 4
        val holder = mockk<TaskAdapter.TaskViewHolder>()
        val binding = mockk<ActivityMainTaskItemBinding>()
        val task = mockk<Task>()
        every { tasks[position] } returns task
        every { holder.binding } returns binding
        every { binding.task = task } answers { nothing }
        every { binding.task } returns task

        // When
        taskAdapter.onBindViewHolder(holder, position)

        // Then
        verify(exactly = 1) { tasks[position] }
        verify(exactly = 1) { binding.task = task }
        assertEquals(task, holder.binding.task)
    }

    @Test
    fun getItemCount() {
        // Given
        every { tasks.size } returns 10

        // When
        val itemCount = taskAdapter.itemCount

        // Then
        assertEquals(10, itemCount)
        verify(exactly = 1) { tasks.size }

    }
}